#include <ImfNamespace.h>
#include <ImfInputFile.h>
#include <ImfOutputFile.h>
#include <ImfHeader.h>
#include <ImfChannelList.h>
#include <ImfChannelListAttribute.h>
#include <ImfFrameBuffer.h>
#include <ImathBox.h>

#include <iostream>
#include <cstring>

namespace IMF = OPENEXR_IMF_NAMESPACE;
using namespace IMF;
using namespace IMATH_NAMESPACE;

int main(int argc, char **argv)
{
  // args
  if (! (argc >= 3))
  {
    std::cerr << "usage: " << argv[0] << " in.exr channel" << std::endl;
    return 1;
  }
  const char *ifilename = argv[1];
  const std::string channel = argv[2];

  uint32_t *uint_data = 0;
  half *half_data = 0;
  float *float_data = 0;
  int type = -1;

  std::fprintf(stderr, "reading %s\n", ifilename);
  InputFile ifile(ifilename);
  const Header &h = ifile.header();
  Box2i dw = h.dataWindow();
  size_t width = dw.max.x - dw.min.x + 1;
  size_t height = dw.max.y - dw.min.y + 1;
  for (Header::ConstIterator i = h.begin(); i != h.end(); ++i)
  {
    const Attribute *a = &i.attribute();
    const ChannelListAttribute *ta = dynamic_cast<const ChannelListAttribute *>(a);
    if (ta)
    {
      const ChannelList &cl = ta->value();
      for (ChannelList::ConstIterator j = cl.begin(); j != cl.end(); ++j)
      {
        if (j.channel().xSampling != 1)
        {
          std::cerr << "ERROR: xSampling != 1" << std::endl;
          return 1;
        }
        if (j.channel().ySampling != 1)
        {
          std::cerr << "ERROR: ySampling != 1" << std::endl;
          return 1;
        }
        if (j.name() == channel)
        {
          type = j.channel().type;
        }
      }
    }
  }
  FrameBuffer ifb;
  switch (type)
  {
    case UINT:
      uint_data = new uint32_t[width * height];
      ifb.insert
        ( channel.c_str()
        , Slice
          ( IMF::UINT
          , (char *) (&uint_data[0] - dw.min.x - dw.min.y * width)
          , sizeof(uint_data[0])
          , sizeof(uint_data[0]) * width
          , 1, 1
          , 0
          )
        );
      ifile.setFrameBuffer(ifb);
      ifile.readPixels(dw.min.y, dw.max.y);
      for (size_t y = 0; y < height; ++y)
      {
        for (size_t x = 0; x < width; ++x)
        {
          if (x > 0)
          {
            std::cout << " ";
          }
          std::cout << uint_data[y * width + x];
        }
        std::cout << std::endl;
      }
      delete[] uint_data;
      break;
    case HALF:
      half_data = new half[width * height];
      ifb.insert
        ( channel.c_str()
        , Slice
          ( IMF::HALF
          , (char *) (&half_data[0] - dw.min.x - dw.min.y * width)
          , sizeof(half_data[0])
          , sizeof(half_data[0]) * width
          , 1, 1
          , 0
          )
        );
      ifile.setFrameBuffer(ifb);
      ifile.readPixels(dw.min.y, dw.max.y);
      for (size_t y = 0; y < height; ++y)
      {
        for (size_t x = 0; x < width; ++x)
        {
          if (x > 0)
          {
            std::cout << " ";
          }
          std::cout << half_data[y * width + x];
        }
        std::cout << std::endl;
      }
      delete[] half_data;
      break;
    case FLOAT:
      float_data = new float[width * height];
      ifb.insert
        ( channel.c_str()
        , Slice
          ( IMF::FLOAT
          , (char *) (&float_data[0] - dw.min.x - dw.min.y * width)
          , sizeof(float_data[0])
          , sizeof(float_data[0]) * width
          , 1, 1
          , 0
          )
       );
      ifile.setFrameBuffer(ifb);
      ifile.readPixels(dw.min.y, dw.max.y);
      for (size_t y = 0; y < height; ++y)
      {
        for (size_t x = 0; x < width; ++x)
        {
          if (x > 0)
          {
            std::cout << " ";
          }
          std::cout << float_data[y * width + x];
        }
        std::cout << std::endl;
      }
      delete[] float_data;
      break;
    default:
      std::cerr << "ERROR: unknown channel type " << type << std::endl;
      return 1;
  }
  return 0;
}
