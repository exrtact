#include <ImfNamespace.h>
#include <ImfInputFile.h>
#include <ImfOutputFile.h>
#include <ImfHeader.h>
#include <ImfChannelList.h>
#include <ImfChannelListAttribute.h>
#include <ImfFrameBuffer.h>
#include <ImathBox.h>

#include <algorithm>
#include <iostream>
#include <cstring>

namespace IMF = OPENEXR_IMF_NAMESPACE;
using namespace IMF;
using namespace IMATH_NAMESPACE;

int main(int argc, char **argv)
{
  // args
  if (! (argc >= 3))
  {
    std::cerr << "usage: " << argv[0] << " out.exr in1.exr [in2.exr [in3.exr [ ... ]]]" << std::endl;
    return 1;
  }
  const char *channel[3] = { "R", "G", "B" };
  const char *ofilename = argv[1];
  half *half_data = 0;
  float *float_data = 0;
  size_t width = 0;
  size_t height = 0;
  for (int arg = 2; arg < argc; ++arg)
  {
    const char *ifilename = argv[arg];
    std::fprintf(stderr, "reading %s\n", ifilename);
    InputFile ifile(ifilename);
    const Header &h = ifile.header();
    Box2i dw = h.dataWindow();
    size_t fwidth = dw.max.x - dw.min.x + 1;
    size_t fheight = dw.max.y - dw.min.y + 1;
    if (width && height && (fwidth != width || fheight != height))
    {
      std::fprintf(stderr, "ERROR: image dimension mismatch\n");
      return 1;
    }
    if (! width || ! height)
    {
      // allocate arrays
      width = fwidth;
      height = fheight;
      half_data = new half[width * height * 3];
      float_data = new float[width * height * 3];
      std::memset(float_data, 0, sizeof(float_data[0]) * width * height * 3);
    }
    // read image
    FrameBuffer ifb;
    for (int c = 0; c < 3; ++c)
    {
      ifb.insert
        ( channel[c]
        , Slice
          ( IMF::HALF
          , (char *) (&half_data[0] + c - dw.min.x - dw.min.y * width)
          , sizeof(half_data[0]) * 3
          , sizeof(half_data[0]) * width * 3
          , 1, 1
          , 0
          )
        );
    }
    ifile.setFrameBuffer(ifb);
    ifile.readPixels(dw.min.y, dw.max.y);
    // accumulate
    #pragma omp parallel for
    for (size_t ix = 0; ix < width * height * 3; ++ix)
    {
      float_data[ix] += half_data[ix];
    }
  }
  // average
  float g = 1.0f / (argc - 2);
  #pragma omp parallel for
  for (size_t ix = 0; ix < width * height * 3; ++ix)
  {
    half_data[ix] = float_data[ix] * g;
  }
  // write image
  std::fprintf(stderr, "writing %s\n", ofilename);
  Header oh(width, height);
  for (int c = 0; c < 3; ++c)
  {
    oh.channels().insert(channel[c], Channel(IMF::HALF));
  }
  OutputFile ofile(ofilename, oh);
  FrameBuffer ofb;
  for (int c = 0; c < 3; ++c)
  {
    ofb.insert(channel[c], Slice(IMF::HALF, (char *) (&half_data[0] + c), sizeof(half_data[0]) * 3, sizeof(half_data[0]) * width * 3));
  }
  ofile.setFrameBuffer(ofb);
  ofile.writePixels(height);
  return 0;
}
