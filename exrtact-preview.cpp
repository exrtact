#include <ImfNamespace.h>
#include <ImfInputFile.h>
#include <ImfHeader.h>
#include <ImfPreviewImage.h>

#include <zlib.h>
#include <png.h>

#include <cstdio>

namespace IMF = OPENEXR_IMF_NAMESPACE;
using namespace IMF;
using namespace IMATH_NAMESPACE;

int main(int argc, char **argv)
{
  // args
  if (argc != 3)
  {
    fprintf(stderr, "usage: %s in.exr out.png\n", argv[0]);
    return 1;
  }
  const char *ifilename = argv[1];
  const char *ofilename = argv[2];
  // read
  InputFile ifile(ifilename);
  auto i = ifile.header().previewImage();
  auto w = i.width();
  auto h = i.height();
  auto p = i.pixels();
  auto rows = new unsigned char *[h];
  for (unsigned int y = 0; y < h; ++y)
    rows[y] = (unsigned char *) (p + y * w);
  // write
  FILE *file = fopen(ofilename, "wb");
  if (! file)
    return 1;
  jmp_buf jmpbuf;
  png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, &jmpbuf, nullptr, nullptr);
  if (! png)
    return 1;
  png_infop info = png_create_info_struct(png);
  if (! info)
  {
    png_destroy_write_struct(&png, 0);
    fclose(file);
    return 1;
  }
  if (setjmp(jmpbuf))
  {
    png_destroy_write_struct(&png, &info);
    fclose(file);
    return 1;
  }
  png_init_io(png, file);
  png_set_compression_level(png, Z_BEST_COMPRESSION);
  png_set_IHDR(png, info, w, h, 8, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_ADAM7, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
  png_set_gAMA(png, info, 2.2);
  png_write_info(png, info);
  png_write_image(png, rows);
  png_write_end(png, 0);
  // cleanup
  png_destroy_write_struct(&png, &info);
  fclose(file);
  delete [] rows;
  return 0;
}
