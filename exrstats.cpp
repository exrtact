#include <ImfNamespace.h>
#include <ImfInputFile.h>
#include <ImfOutputFile.h>
#include <ImfHeader.h>
#include <ImfChannelList.h>
#include <ImfChannelListAttribute.h>
#include <ImfFrameBuffer.h>
#include <ImathBox.h>

#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <thread>

namespace IMF = OPENEXR_IMF_NAMESPACE;
using namespace IMF;
using namespace IMATH_NAMESPACE;

template <typename T>
std::set<T> intersect(std::set<T> &s1, std::set<T> &s2)
{
  std::set<T> intersection;
  std::set_intersection
    ( s1.begin(), s1.end()
    , s2.begin(), s2.end()
    , std::inserter(intersection, intersection.begin())
    );
  return intersection;
}

template<typename T>
void stats(const std::string &name, const T *ptr, size_t xstride, size_t width, size_t height)
{
  size_t ystride = xstride * width;
  T minimum = 1.0 / 0.0;
  if (minimum == 0)
  {
    minimum = UINT_MAX;
  }
  T maximum = -1.0 / 0.0;
  double sum = 0.0;
  for (size_t y = 0; y < height; ++y)
  {
    for (size_t x = 0; x < width; ++x)
    {
      T sample = ptr[xstride * x + ystride * y];
      minimum = std::min(minimum, sample);
      maximum = std::max(maximum, sample);
      sum = sum + sample;
    }
  }
  double mean = sum / (width * height);
  double variance = 0.0;
  for (size_t y = 0; y < height; ++y)
  {
    for (size_t x = 0; x < width; ++x)
    {
      T sample = ptr[xstride * x + ystride * y];
      double deviation = sample - mean;
      variance = variance + deviation * deviation;
    }
  }
  variance = variance / (width * height);
  double stddev = std::sqrt(variance) * (width * height) / (width * height - 1);
  std::cout << name << "\t" << minimum << "\t" << maximum << "\t" << mean << "\t" << stddev << std::endl;
}

int main(int argc, char **argv)
{
  // args
  if (! (argc >= 2))
  {
    std::cerr << "usage: " << argv[0] << " in.exr [channel [channel [...]]]" << std::endl;
    return 1;
  }
  const char *ifilename = argv[1];
  std::set<std::string> keep_channels;
  for (int i = 2; i < argc; ++i)
  {
    keep_channels.insert(argv[i]);
  }
  std::set<std::string> uint_channels, half_channels, float_channels;
  uint32_t *uint_data = 0;
  half *half_data = 0;
  float *float_data = 0;
  size_t width = 0;
  size_t height = 0;

  setGlobalThreadCount(std::thread::hardware_concurrency());

  // analyze channels
  std::fprintf(stderr, "reading %s\n", ifilename);
  InputFile ifile(ifilename);
  const Header &h = ifile.header();
  Box2i dw = h.dataWindow();
  size_t fwidth = dw.max.x - dw.min.x + 1;
  size_t fheight = dw.max.y - dw.min.y + 1;
  std::set<std::string> fuint_channels, fhalf_channels, ffloat_channels;
  for (Header::ConstIterator i = h.begin(); i != h.end(); ++i)
  {
    const Attribute *a = &i.attribute();
    const ChannelListAttribute *ta = dynamic_cast<const ChannelListAttribute *>(a);
    if (ta)
    {
      const ChannelList &cl = ta->value();
      for (ChannelList::ConstIterator j = cl.begin(); j != cl.end(); ++j)
      {
        if (j.channel().xSampling != 1)
        {
          std::cerr << "ERROR: xSampling != 1" << std::endl;
          return 1;
        }
        if (j.channel().ySampling != 1)
        {
          std::cerr << "ERROR: ySampling != 1" << std::endl;
          return 1;
        }
        switch (j.channel().type)
        {
          case UINT:
            fuint_channels.insert(j.name());
            break;
          case HALF:
            fhalf_channels.insert(j.name());
            break;
          case FLOAT:
            ffloat_channels.insert(j.name());
            break;
          default:
            std::cerr << "ERROR: unknown channel type " << j.channel().type << std::endl;
            return 1;
        }
      }
      fuint_channels = keep_channels.size() > 0 ? intersect(keep_channels, fuint_channels) : fuint_channels;
      fhalf_channels = keep_channels.size() > 0 ? intersect(keep_channels, fhalf_channels) : fhalf_channels;
      ffloat_channels = keep_channels.size() > 0 ? intersect(keep_channels, ffloat_channels) : ffloat_channels;

      width = fwidth;
      height = fheight;
      uint_channels = fuint_channels;
      half_channels = fhalf_channels;
      float_channels = ffloat_channels;
      if (uint_channels.size() > 0)
      {
        uint_data = new uint32_t[uint_channels.size() * width * height];
      }
      if (half_channels.size() > 0)
      {
        half_data = new half[half_channels.size() * width * height];
      }
      if (float_channels.size() > 0)
      {
        float_data = new float[float_channels.size() * width * height];
      }
    }
  }

  if (! (uint_channels.size() + half_channels.size() + float_channels.size() > 0))
  {
    std::cerr << "ERROR: no channels" << std::endl;
    return 1;
  }

  // read image
  FrameBuffer ifb;
  int k = 0;
  for (auto name : uint_channels)
  {
    ifb.insert
      ( name.c_str()
      , Slice
        ( IMF::UINT
        , (char *) (&uint_data[0] + k - dw.min.x - dw.min.y * width)
        , sizeof(uint_data[0]) * uint_channels.size()
        , sizeof(uint_data[0]) * width * uint_channels.size()
        , 1, 1
        , 0
        )
     );
    ++k;
  }
  k = 0;
  for (auto name : half_channels)
  {
    ifb.insert
      ( name.c_str()
      , Slice
        ( IMF::HALF
        , (char *) (&half_data[0] + k - dw.min.x - dw.min.y * width)
        , sizeof(half_data[0]) * half_channels.size()
        , sizeof(half_data[0]) * width * half_channels.size()
        , 1, 1
        , 0
        )
      );
    ++k;
  }
  k = 0;
  for (auto name : float_channels)
  {
    ifb.insert
      ( name.c_str()
      , Slice
        ( IMF::FLOAT
        , (char *) (&float_data[0] + k - dw.min.x - dw.min.y * width)
        , sizeof(float_data[0]) * float_channels.size()
        , sizeof(float_data[0]) * width * float_channels.size()
        , 1, 1
        , 0
        )
     );
    ++k;
  }
  ifile.setFrameBuffer(ifb);
  ifile.readPixels(dw.min.y, dw.max.y);

  // compute and report statistics
  std::cout << "# name\tminimum\tmaximum\tmean\tstddev" << std::endl;
  k = 0;
  for (auto name : uint_channels)
  {
    stats(name, &uint_data[0] + k, uint_channels.size(), width, height);
    ++k;
  }
  k = 0;
  for (auto name : half_channels)
  {
    stats(name, &half_data[0] + k, half_channels.size(), width, height);
    ++k;
  }
  k = 0;
  for (auto name : float_channels)
  {
    stats(name, &float_data[0] + k, float_channels.size(), width, height);
    ++k;
  }
  return 0;
}
