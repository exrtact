# exrtact

various tools to manipulate EXR images  
<https://mathr.co.uk/exrtact>  
<https://code.mathr.co.uk/exrtact>

## get

    sudo apt install git make g++ pkg-config libopenexr-dev libpng-dev
    git clone https://code.mathr.co.uk/exrtact.git
    cd exrtact
    make

## usage

### channel extraction

    exrtact in.exr out.exr chan

- `in.exr` is the input file, it should have a channel named `chan`
  which will be extracted
- `out.exr` is the output file which will be created, it will have
  a channel named `Y` which contains the extracted channel

### preview extraction

    exrtact-preview in.exr out.png

- `in.exr` is the input file, it should have a preview image
  which will be extracted
- `out.png` is the output file which will be created, it will have
  RGBA8 channels containing the extracted preview image

### image tiling

    exrtactile stem factor stratify out.exr [channel [channel [...]]]

- combines `stem-YYYY-XXXX.exr` into `out.exr`
  where each `XXXX`, `YYYY` is zero-padded
  4 decimal digits from `0` to `factor - 1`.
  `X` increases left to right.
  `Y` increases top to bottom.
- if `stratify` is `0`, does regular tiling
  (the images are placed side by side)
- if `stratify` is `1`, does "stratified" tiling
  (the pixels get interleaved)
- if channel list is specified, only those channels are used,
  otherwise all channels are used

### image reduction

    exrsubsample factor in.exr out.exr [channel [channel [...]]]

- subsamples `in.exr` by `factor` into `out.exr`
- if channel list is specified, only those channels are used,
  otherwise all channels are used

### histogram quantiles

    exrquantile nbins in.exr channel

- sorts the data of channel, groups it into bins with equal sizes,
  and outputs the quantities at bin boundaries
- output is tab separated values with header
- uint32, float32, float16 (half) data are supported

### channel statistics

    exrstats in.exr [channel [channel [...]]]

- outputs minimum, maximum, mean, standard deviation of each channel
- output is tab separated values with header
- uint32, float32, float16 (half) data are supported
- if channel list is specified, only those channels are used,
  otherwise all channels are used

### image averaging

    exrmean out.exr in1.exr [in2.exr [in3.exr [... ]]]

- averages R G B input images (arithmetic mean)
- other channels are ignored
- all images must have the same dimensions

### plain text conversion

    exrplain in.exr channel > out.txt

- prints channel data as decimal numbers

### distance estimate equalisation

    exrdeeq in.exr out.exr

- reads DEX and DEY channels
- outputs Y channel
- |DE| <= 0 becomes 0
- |DE| >= 1 becomes 1
- otherwise Y is the index of |DE| in the sorted array, scaled lie in to 0..1

## bugs

- no license

### channel extraction

- only single part scanline flat images are supported
- only float32 channels are supported
  (OpenEXR may cast other types on read, potentially losing data
  or expanding space usage for no gain)

### preview extraction

- only single part images are supported

### image tiling

- preview images are not processed
- only single part scanline flat images are supported
- only one factor for both dimensions

### image reduction

- preview images are not processed
- only single part scanline flat images are supported
- only one factor for both dimensions
- no filtering before decimation
- no control over which subpixel is selected (always top left)

### histogram quantiles

- float16 (half) data is output with misleadingly high precision

### image averaging

- only float16 R, G, B is supported

### plain text conversion

- float16 (half) data is output with misleadingly high precision

### distance estimate equalisation

- just sorts the data, instead of using histograms with bigger buckets
- thresholds are not configurable
