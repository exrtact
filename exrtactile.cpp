#include <ImfNamespace.h>
#include <ImfInputFile.h>
#include <ImfOutputFile.h>
#include <ImfHeader.h>
#include <ImfChannelList.h>
#include <ImfChannelListAttribute.h>
#include <ImfFrameBuffer.h>
#include <ImathBox.h>

#include <algorithm>
#include <iostream>
#include <cstring>

namespace IMF = OPENEXR_IMF_NAMESPACE;
using namespace IMF;
using namespace IMATH_NAMESPACE;

template <typename T>
std::set<T> intersect(std::set<T> &s1, std::set<T> &s2)
{
  std::set<T> intersection;
  std::set_intersection
    ( s1.begin(), s1.end()
    , s2.begin(), s2.end()
    , std::inserter(intersection, intersection.begin())
    );
  return intersection;
}

int main(int argc, char **argv)
{
  // args
  if (! (argc >= 5))
  {
    std::cerr << "usage: " << argv[0] << " stem factor stratify out.exr [channel [channel [...]]]" << std::endl;
    return 1;
  }
  const char *istem = argv[1];
  const int factor = atoi(argv[2]);
  const bool stratify = atoi(argv[3]);
  const char *ofilename = argv[4];
  std::set<std::string> keep_channels;
  for (int i = 5; i < argc; ++i)
  {
    keep_channels.insert(argv[i]);
  }

  std::set<std::string> uint_channels, half_channels, float_channels;
  uint32_t *uint_data = 0;
  half *half_data = 0;
  float *float_data = 0;
  size_t width = 0;
  size_t height = 0;

  for (int y = 0; y < factor; ++y)
  {
    for (int x = 0; x < factor; ++x)
    {
      char ifilename[1000];
      std::snprintf(ifilename, 999, "%s-%04d-%04d.exr", istem, y, x);
      std::fprintf(stderr, "reading %s\n", ifilename);
      InputFile ifile(ifilename);
      const Header &h = ifile.header();
      Box2i dw = h.dataWindow();
      size_t fwidth = dw.max.x - dw.min.x + 1;
      size_t fheight = dw.max.y - dw.min.y + 1;
      std::set<std::string> fuint_channels, fhalf_channels, ffloat_channels;
      for (Header::ConstIterator i = h.begin(); i != h.end(); ++i)
      {
        const Attribute *a = &i.attribute();
        const ChannelListAttribute *ta = dynamic_cast<const ChannelListAttribute *>(a);
        if (ta)
        {
          const ChannelList &cl = ta->value();
          for (ChannelList::ConstIterator j = cl.begin(); j != cl.end(); ++j)
          {
            if (j.channel().xSampling != 1)
            {
              std::cerr << "ERROR: xSampling != 1" << std::endl;
              return 1;
            }
            if (j.channel().ySampling != 1)
            {
              std::cerr << "ERROR: ySampling != 1" << std::endl;
              return 1;
            }
            switch (j.channel().type)
            {
              case UINT:
                fuint_channels.insert(j.name());
                break;
              case HALF:
                fhalf_channels.insert(j.name());
                break;
              case FLOAT:
                ffloat_channels.insert(j.name());
                break;
              default:
                std::cerr << "ERROR: unknown channel type " << j.channel().type << std::endl;
                return 1;
            }
          }
          fuint_channels = keep_channels.size() > 0 ? intersect(keep_channels, fuint_channels) : fuint_channels;
          fhalf_channels = keep_channels.size() > 0 ? intersect(keep_channels, fhalf_channels) : fhalf_channels;
          ffloat_channels = keep_channels.size() > 0 ? intersect(keep_channels, ffloat_channels) : ffloat_channels;

          if (x == 0 && y == 0)
          {
            width = fwidth;
            height = fheight;
            uint_channels = fuint_channels;
            half_channels = fhalf_channels;
            float_channels = ffloat_channels;
            if (uint_channels.size() > 0)
            {
              uint_data = new uint32_t[uint_channels.size() * factor * width * factor * height];
            }
            if (half_channels.size() > 0)
            {
              half_data = new half[half_channels.size() * factor * width * factor * height];
            }
            if (float_channels.size() > 0)
            {
              float_data = new float[float_channels.size() * factor * width * factor * height];
            }
          }
          else
          {
            if (width != fwidth || height != fheight || uint_channels != fuint_channels || half_channels != fhalf_channels || float_channels != ffloat_channels)
            {
              std::cerr << "incompatible files" << std::endl;
              return 1;
            }
          }
        }
      }

      // read image
      FrameBuffer ifb;
      if (stratify)
      {
        int k = 0;
        for (auto name : uint_channels)
        {
          ifb.insert
            ( name.c_str()
            , Slice
             ( IMF::UINT
              , (char *) (&uint_data[0] + k - dw.min.x * factor - dw.min.y * factor * width + x * uint_channels.size() + y * uint_channels.size() * width * factor)
              , sizeof(uint_data[0]) * factor * uint_channels.size()
              , sizeof(uint_data[0]) * factor * factor * width * uint_channels.size()
              , 1, 1
              , 0
              )
            );
          ++k;
        }
        k = 0;
        for (auto name : half_channels)
        {
          ifb.insert
            ( name.c_str()
            , Slice
              ( IMF::HALF
              , (char *) (&half_data[0] + k - dw.min.x * factor - dw.min.y * factor * width + x * half_channels.size() + y * half_channels.size() * width * factor)
              , sizeof(half_data[0]) * factor * half_channels.size()
              , sizeof(half_data[0]) * factor * factor * width * half_channels.size()
              , 1, 1
              , 0
              )
            );
          ++k;
        }
        k = 0;
        for (auto name : float_channels)
        {
          ifb.insert
            ( name.c_str()
            , Slice
              ( IMF::FLOAT
              , (char *) (&float_data[0] + k - dw.min.x * factor - dw.min.y * factor * width + x * float_channels.size() + y * float_channels.size() * width * factor)
              , sizeof(float_data[0]) * factor * float_channels.size()
              , sizeof(float_data[0]) * factor * factor * width * float_channels.size()
              , 1, 1
              , 0
              )
            );
          ++k;
        }
      }
      else
      {
        int k = 0;
        for (auto name : uint_channels)
        {
          ifb.insert
            ( name.c_str()
            , Slice
              ( IMF::UINT
              , (char *) (&uint_data[0] + k - dw.min.x - dw.min.y * width + x * uint_channels.size() * width + y * uint_channels.size() * factor * width * height)
              , sizeof(uint_data[0]) * uint_channels.size()
              , sizeof(uint_data[0]) * factor * width * uint_channels.size()
              , 1, 1
              , 0
              )
           );
          ++k;
        }
        k = 0;
        for (auto name : half_channels)
        {
          ifb.insert
            ( name.c_str()
            , Slice
              ( IMF::HALF
              , (char *) (&half_data[0] + k - dw.min.x - dw.min.y * width + x * half_channels.size() * width + y * half_channels.size() * factor * width * height)
              , sizeof(half_data[0]) * half_channels.size()
              , sizeof(half_data[0]) * factor * width * half_channels.size()
              , 1, 1
              , 0
              )
            );
          ++k;
        }
        k = 0;
        for (auto name : float_channels)
        {
          ifb.insert
            ( name.c_str()
            , Slice
              ( IMF::FLOAT
              , (char *) (&float_data[0] + k - dw.min.x - dw.min.y * width + x * float_channels.size() * width + y * float_channels.size() * factor * width * height)
              , sizeof(float_data[0]) * float_channels.size()
              , sizeof(float_data[0]) * factor * width * float_channels.size()
              , 1, 1
              , 0
              )
           );
          ++k;
        }
      }
      ifile.setFrameBuffer(ifb);
      ifile.readPixels(dw.min.y, dw.max.y);
    }
  }

  // write
  std::fprintf(stderr, "writing %s\n", ofilename);
  Header oh(width * factor, height * factor);
  for (auto name : uint_channels)
  {
    oh.channels().insert(name.c_str(), Channel(IMF::UINT));
  }
  for (auto name : half_channels)
  {
    oh.channels().insert(name.c_str(), Channel(IMF::HALF));
  }
  for (auto name : float_channels)
  {
    oh.channels().insert(name.c_str(), Channel(IMF::FLOAT));
  }
  OutputFile ofile(ofilename, oh);
  FrameBuffer ofb;
  int k = 0;
  for (auto name : uint_channels)
  {
    ofb.insert(name, Slice(IMF::UINT, (char *) (&uint_data[0] + k), sizeof(uint_data[0]) * uint_channels.size(), sizeof(uint_data[0]) * factor * width * uint_channels.size()));
    ++k;
  }
  k = 0;
  for (auto name : half_channels)
  {
    ofb.insert(name, Slice(IMF::HALF, (char *) (&half_data[0] + k), sizeof(half_data[0]) * half_channels.size(), sizeof(half_data[0]) * factor * width * half_channels.size()));
    ++k;
  }
  k = 0;
  for (auto name : float_channels)
  {
    ofb.insert(name, Slice(IMF::FLOAT, (char *) (&float_data[0] + k), sizeof(float_data[0]) * float_channels.size(), sizeof(float_data[0]) * factor * width * float_channels.size()));
    ++k;
  }
  ofile.setFrameBuffer(ofb);
  ofile.writePixels(factor * height);
  return 0;
}
