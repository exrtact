#include <ImfNamespace.h>
#include <ImfInputFile.h>
#include <ImfOutputFile.h>
#include <ImfHeader.h>
#include <ImfChannelList.h>
#include <ImfFrameBuffer.h>
#include <ImathBox.h>

#include <iostream>

namespace IMF = OPENEXR_IMF_NAMESPACE;
using namespace IMF;
using namespace IMATH_NAMESPACE;

int main(int argc, char **argv)
{
  // args
  if (argc != 3)
  {
    std::cerr << "usage: " << argv[0] << " in.exr channel" << std::endl;
    return 1;
  }
  const char *ifilename = argv[1];
  const char *ichannel = argv[2];
  // read
  InputFile ifile(ifilename);
  const Header &h = ifile.header();
  Box2i dw = h.dataWindow();
  ssize_t width = dw.max.x - dw.min.x + 1;
  ssize_t height = dw.max.y - dw.min.y + 1;
  half *Y = new half[width * height];
  FrameBuffer ifb;
  ifb.insert(ichannel, Slice(IMF::HALF, (char *) (&Y[0] - dw.min.x - dw.min.y * width), sizeof(Y[0]) * 1, sizeof(Y[0]) * width, 1, 1, 0));
  ifile.setFrameBuffer(ifb);
  ifile.readPixels(dw.min.y, dw.max.y);
  // analyse
  ssize_t n_nan = 0, n_pinf = 0, n_ninf = 0, n_zero = 0, n_neg = 0, n_sub = 0, n_norm = 0;
  for (ssize_t k = 0; k < width * height; ++k)
  {
    double y = Y[k];
    if (isnan(y)) n_nan++;
    else if (isinf(y) && y > 0) n_pinf++;
    else if (isinf(y) && y < 0) n_ninf++;
    else if (y == 0) n_zero++;
    else if (y < 0) n_neg++;
    else if (y < pow(2, -14)) n_sub++;
    else n_norm++;
  }
  delete[] Y;
  std::cout
    << "nan\t" << n_nan << "\n"
    << "+inf\t" << n_pinf << "\n"
    << "-inf\t" << n_ninf << "\n"
    << "0.0\t" << n_zero << "\n"
    << "<0\t" << n_neg << "\n"
    << "sub\t" << n_sub << "\n"
    << "ok\t" << n_norm << "\n"
    ;
  return 0;
}
