#include <ImfNamespace.h>
#include <ImfInputFile.h>
#include <ImfOutputFile.h>
#include <ImfHeader.h>
#include <ImfChannelList.h>
#include <ImfChannelListAttribute.h>
#include <ImfFrameBuffer.h>
#include <ImathBox.h>

#include <iostream>
#include <cstdlib>
#include <cstring>

namespace IMF = OPENEXR_IMF_NAMESPACE;
using namespace IMF;
using namespace IMATH_NAMESPACE;

struct pixel
{
  float de;
  int ix;
};

int cmp_de(const void *a, const void *b)
{
  const pixel *x = (const pixel *) a;
  const pixel *y = (const pixel *) b;
  return (x->de > y->de) - (y->de > x->de);
}

int cmp_ix(const void *a, const void *b)
{
  const pixel *x = (const pixel *) a;
  const pixel *y = (const pixel *) b;
  return (x->ix > y->ix) - (y->ix > x->ix);
}

int main(int argc, char **argv)
{
  // args
  if (! (argc >= 3))
  {
    std::cerr << "usage: " << argv[0] << " in.exr out.exr" << std::endl;
    return 1;
  }
  const char *ifilename = argv[1];
  const char *ofilename = argv[2];

  half *half_data = 0;
  float *float_data = 0;
  int type = -1;

  std::fprintf(stderr, "reading %s\n", ifilename);
  InputFile ifile(ifilename);
  const Header &h = ifile.header();
  Box2i dw = h.dataWindow();
  size_t width = dw.max.x - dw.min.x + 1;
  size_t height = dw.max.y - dw.min.y + 1;
  for (Header::ConstIterator i = h.begin(); i != h.end(); ++i)
  {
    const Attribute *a = &i.attribute();
    const ChannelListAttribute *ta = dynamic_cast<const ChannelListAttribute *>(a);
    if (ta)
    {
      const ChannelList &cl = ta->value();
      for (ChannelList::ConstIterator j = cl.begin(); j != cl.end(); ++j)
      {
        if (j.channel().xSampling != 1)
        {
          std::cerr << "ERROR: xSampling != 1" << std::endl;
          return 1;
        }
        if (j.channel().ySampling != 1)
        {
          std::cerr << "ERROR: ySampling != 1" << std::endl;
          return 1;
        }
        if (std::string(j.name()) == "DEX" || std::string(j.name()) == "DEY")
        {
          if (type == -1)
          {
            type = j.channel().type;
          }
          else
          {
            if (type != j.channel().type)
            {
              std::cerr << "ERROR: mismatched types" << std::endl;
              return 1;
            }
          }
        }
      }
    }
  }

  // convert DEX + DEY to |DE|
  FrameBuffer ifb;
  pixel *pixels = new pixel[width * height];
  switch (type)
  {
    case HALF:
      half_data = new half[2 * width * height];
      ifb.insert
        ( "DEX"
        , Slice
          ( IMF::HALF
          , (char *) (&half_data[0] - dw.min.x * 2 - dw.min.y * width * 2)
          , sizeof(half_data[0]) * 2
          , sizeof(half_data[0]) * width * 2
          , 1, 1
          , 0
          )
        );
      ifb.insert
        ( "DEY"
        , Slice
          ( IMF::HALF
          , (char *) (&half_data[0] - dw.min.x * 2 - dw.min.y * width * 2 + 1)
          , sizeof(half_data[0]) * 2
          , sizeof(half_data[0]) * width * 2
          , 1, 1
          , 0
          )
        );
      ifile.setFrameBuffer(ifb);
      ifile.readPixels(dw.min.y, dw.max.y);
      #pragma omp parallel for
      for (size_t y = 0; y < height; ++y)
      {
        for (size_t x = 0; x < width; ++x)
        {
          size_t ix = y * width + x;
          half dex = half_data[2 * ix + 0];
          half dey = half_data[2 * ix + 1];
          pixels[ix].de = sqrt(dex * dex + dey * dey);
          pixels[ix].ix = ix;
        }
      }
      delete[] half_data;
      break;

    case FLOAT:
      float_data = new float[2 * width * height];
      ifb.insert
        ( "DEX"
        , Slice
          ( IMF::FLOAT
          , (char *) (&float_data[0] - dw.min.x * 2 - dw.min.y * width * 2)
          , sizeof(float_data[0]) * 2
          , sizeof(float_data[0]) * width * 2
          , 1, 1
          , 0
          )
        );
      ifb.insert
        ( "DEY"
        , Slice
          ( IMF::FLOAT
          , (char *) (&float_data[0] - dw.min.x * 2 - dw.min.y * width * 2 + 1)
          , sizeof(float_data[0]) * 2
          , sizeof(float_data[0]) * width * 2
          , 1, 1
          , 0
          )
        );
      ifile.setFrameBuffer(ifb);
      ifile.readPixels(dw.min.y, dw.max.y);
      #pragma omp parallel for
      for (size_t y = 0; y < height; ++y)
      {
        for (size_t x = 0; x < width; ++x)
        {
          size_t ix = y * width + x;
          float dex = float_data[2 * ix + 0];
          float dey = float_data[2 * ix + 1];
          pixels[ix].de = sqrt(dex * dex + dey * dey);
          pixels[ix].ix = ix;
        }
      }
      delete[] float_data;
      break;

    default:
      std::cerr << "ERROR: unsupported channel type " << type << std::endl;
      return 1;
  }

  // equalize
  qsort(pixels, width * height, sizeof(*pixels), cmp_de);
  float lower_threshold = 0; // exclude interior (de = 0)
  size_t lo = 0;
  size_t hi = width * height;
  size_t md = (lo + hi) / 2;
  while (lo + 1 < hi)
  {
    if (pixels[md].de > lower_threshold)
    {
      hi = md;
    }
    else
    {
      lo = md;
    }
    md = (lo + hi) / 2;
  }
  size_t lower_index = hi;

  float upper_threshold = 1; // in pixels, TODO make settable
  lo = 0;
  hi = width * height;
  md = (lo + hi) / 2;
  while (lo + 1 < hi)
  {
    if (pixels[md].de > upper_threshold)
    {
      hi = md;
    }
    else
    {
      lo = md;
    }
    md = (lo + hi) / 2;
  }
  size_t upper_index = hi;

  float scale = 1.0 / (upper_index - lower_index);
  #pragma omp parallel for
  for (size_t ix = 0; ix < lower_index; ++ix)
  {
    pixels[ix].de = 0;
  }
  #pragma omp parallel for
  for (size_t ix = lower_index; ix < upper_index; ++ix)
  {
    pixels[ix].de = (ix - lower_index) * scale; // FIXME bigger buckets...
  }
  #pragma omp parallel for
  for (size_t ix = hi; ix < width * height; ++ix)
  {
    pixels[ix].de = 1;
  }
  qsort(pixels, width * height, sizeof(*pixels), cmp_ix);

  // write
  std::fprintf(stderr, "writing %s\n", ofilename);
  Header oh(width, height);
  oh.channels().insert("Y", Channel(IMF::FLOAT));
  OutputFile ofile(ofilename, oh);
  FrameBuffer ofb;
  ofb.insert("Y", Slice(IMF::FLOAT, (char *) (&pixels[0].de), sizeof(pixels[0]) * 1, sizeof(pixels[0]) * width));
  ofile.setFrameBuffer(ofb);
  ofile.writePixels(height);
  delete[] pixels;

  return 0;
}
