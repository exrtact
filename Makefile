WIN32 ?= $(HOME)/win/posix/i686
WIN64 ?= $(HOME)/win/posix/x86_64

all: posix win32 win64

EXE = \
exrdeeq \
exrmean \
exrplain \
exrquantile \
exrstats \
exrsubsample \
exrtact \
exrtactile \
exrtact-preview \
exrtoohot \

EXE_i686 = $(patsubst %,%.i686.exe,$(EXE))
EXE_x86_64 = $(patsubst %,%.x86_64.exe,$(EXE))

posix: $(EXE)
win32: $(EXE_i686)
win64: $(EXE_x86_64)

clean:
	-rm $(EXE)
	-rm $(EXE_i686)
	-rm $(EXE_x86_64)

%: %.cpp
	g++ -std=c++14 -Wall -Wextra -pedantic -Wno-deprecated -fopenmp -O3 -o $@ $< `pkg-config --cflags --libs OpenEXR libpng`

%.i686.exe: %.cpp
	i686-w64-mingw32-g++   -std=c++14 -Wall -Wextra -pedantic -Wno-deprecated -O3 -static-libgcc -static-libstdc++ -static -lpthread -Wl,--stack,67108864 -I$(WIN32)/include -I$(WIN32)/include/OpenEXR -L$(WIN32)/lib -o $@ $< -lIlmImf-2_5 -lImath-2_5 -lHalf-2_5 -lIex-2_5 -lIexMath-2_5 -lIlmThread-2_5 -lpng -lz -lpthread -D_FILE_OFFSET_BITS=64

%.x86_64.exe: %.cpp
	x86_64-w64-mingw32-g++ -std=c++14 -Wall -Wextra -pedantic -Wno-deprecated -O3 -static-libgcc -static-libstdc++ -static -lpthread -Wl,--stack,67108864 -I$(WIN64)/include -I$(WIN64)/include/OpenEXR -L$(WIN64)/lib -o $@ $< -lIlmImf-2_5 -lImath-2_5 -lHalf-2_5 -lIex-2_5 -lIexMath-2_5 -lIlmThread-2_5 -lpng -lz -lpthread -D_FILE_OFFSET_BITS=64
