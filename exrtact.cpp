#include <ImfNamespace.h>
#include <ImfInputFile.h>
#include <ImfOutputFile.h>
#include <ImfHeader.h>
#include <ImfChannelList.h>
#include <ImfFrameBuffer.h>
#include <ImathBox.h>

#include <iostream>

namespace IMF = OPENEXR_IMF_NAMESPACE;
using namespace IMF;
using namespace IMATH_NAMESPACE;

int main(int argc, char **argv)
{
  // args
  if (argc != 4)
  {
    std::cerr << "usage: " << argv[0] << " in.exr out.exr channel" << std::endl;
    return 1;
  }
  const char *ifilename = argv[1];
  const char *ofilename = argv[2];
  const char *ichannel = argv[3];
  const char *ochannel = "Y";
  // read
  InputFile ifile(ifilename);
  const Header &h = ifile.header();
  Box2i dw = h.dataWindow();
  size_t width = dw.max.x - dw.min.x + 1;
  size_t height = dw.max.y - dw.min.y + 1;
  float *Y = new float[width * height];
  FrameBuffer ifb;
  ifb.insert(ichannel, Slice(IMF::FLOAT, (char *) (&Y[0] - dw.min.x - dw.min.y * width), sizeof(Y[0]) * 1, sizeof(Y[0]) * width, 1, 1, 0));
  ifile.setFrameBuffer(ifb);
  ifile.readPixels(dw.min.y, dw.max.y);
  // write
  Header oh(width, height);
  oh.channels().insert(ochannel, Channel(IMF::FLOAT));
  OutputFile ofile(ofilename, oh);
  FrameBuffer ofb;
  ofb.insert(ochannel, Slice(IMF::FLOAT, (char *) (&Y[0]), sizeof(Y[0]) * 1, sizeof(Y[0]) * width));
  ofile.setFrameBuffer(ofb);
  ofile.writePixels(height);
  delete[] Y;
  return 0;
}
